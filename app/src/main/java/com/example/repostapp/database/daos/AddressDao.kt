package com.example.postsapp.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.postsapp.database.models.AddressTable

@Dao
interface AddressDao {

    @Query("SELECT * FROM address_table")
    fun getAll(): MutableList<AddressTable>

    @Query("SELECT * FROM address_table WHERE id=:id")
    fun getById(id: Long): AddressTable

    @Query("SELECT * FROM address_table WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<AddressTable>

    //@Query("SELECT * FROM address_table WHERE name LIKE :name AND " +
    //        "username LIKE :userName LIMIT 1")
    //fun findByName(name: String, userName: String): AddressTable

    @Insert
    fun insert(user: AddressTable): Long

    //@Insert
    //fun insertAll(vararg users: UsersTable)

    @Delete
    fun delete(user: AddressTable)
}