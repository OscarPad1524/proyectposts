package com.example.postsapp.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.postsapp.database.models.PostsTable
import kotlinx.coroutines.flow.Flow

@Dao
interface PostsDao {
    @Query("SELECT * FROM posts_table")
    suspend fun getAll(): MutableList<PostsTable>

    @Query("SELECT * FROM posts_table WHERE id=:id")
    fun getById(id: Long): PostsTable

    @Query("SELECT * FROM posts_table WHERE id_server=:id")
    fun getByIdServer(id: Long): PostsTable

    @Query("SELECT * FROM posts_table WHERE id_user=:id")
    suspend fun getAllById(id: String): MutableList<PostsTable>

    @Query("SELECT * FROM posts_table WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<PostsTable>

    //@Query("SELECT * FROM geos_table WHERE name LIKE :name AND " +
    //        "username LIKE :userName LIMIT 1")
    //fun findByName(name: String, userName: String): GeosTable

    @Query("SELECT * FROM posts_table WHERE id_user = :userId")
    fun getFlow(userId: String): Flow<List<PostsTable>>

    @Query("SELECT * FROM posts_table WHERE id_user = :userId")
    fun getFlujo(userId: String): Flow<List<PostsTable>>

    @Insert
    fun insert(user: PostsTable): Long

    @Insert
    fun insertAll(vararg users: PostsTable)

    @Delete
    fun delete(user: PostsTable)
}