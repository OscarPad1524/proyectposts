package com.example.repostapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users_table")
data class UsersTable(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id: Long = 0,
    @ColumnInfo(name = "id_remote") var id_server: String? = "",
    @ColumnInfo(name = "name") var name: String? = "",
    @ColumnInfo(name = "username") var username: String? = "",
    @ColumnInfo(name = "email") var email: String? = "",
    @ColumnInfo(name = "id_address") var id_address: Long? = 0,
    @ColumnInfo(name = "phone") var phone: String? = "",
    @ColumnInfo(name = "web_site") var website: String? = "",
    @ColumnInfo(name = "id_company") var id_company: Long? = 0
)