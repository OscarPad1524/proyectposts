package com.example.postsapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "companys_table")
data class CompanysTable (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id: Long = 0,
    @ColumnInfo(name = "id_remote") var id_server: String? = "",
    @ColumnInfo(name = "id_user") var id_user: String? = "",
    @ColumnInfo(name = "bs") var bs: String? = "",
    @ColumnInfo(name = "catch_phrase") var catch_phrase: String? = "",
    @ColumnInfo(name = "name") var name: String? = ""

)