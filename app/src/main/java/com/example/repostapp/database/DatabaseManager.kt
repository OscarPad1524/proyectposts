package com.example.repostapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.postsapp.database.dao.AddressDao
import com.example.postsapp.database.dao.CompanysDao
import com.example.postsapp.database.dao.GeosDao
import com.example.postsapp.database.dao.PostsDao
import com.example.postsapp.database.models.AddressTable
import com.example.postsapp.database.models.CompanysTable
import com.example.postsapp.database.models.GeosTable
import com.example.postsapp.database.models.PostsTable
import com.example.repostapp.database.daos.UsersDao
import com.example.repostapp.database.models.UsersTable

@Database(
    entities = [
        UsersTable::class,
        GeosTable::class,
        AddressTable::class,
        CompanysTable::class,
        PostsTable::class
    ],
    version = 1
)
abstract class DatabaseManager: RoomDatabase() {

    abstract fun usersDao(): UsersDao
    abstract fun geosDao(): GeosDao
    abstract fun companysDao(): CompanysDao
    abstract fun addreesDao(): AddressDao
    abstract fun postsDao(): PostsDao

    companion object {
        @Volatile
        private var INSTANCE: DatabaseManager? = null

        @JvmStatic
        fun getDatabase(context: Context): DatabaseManager {
            val tempInstance = INSTANCE

            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DatabaseManager::class.java, "AppDatabase.db"
                ).build()

                INSTANCE = instance

                return instance
            }
        }
    }

}