package com.example.postsapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.coroutines.flow.Flow

@Entity(tableName = "posts_table")
data class PostsTable(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id: Long = 0,
    @ColumnInfo(name = "id_user") var id_user: String? = "",
    @ColumnInfo(name = "id_server") var id_server: Long? = 0,
    @ColumnInfo(name = "tittle") var tittle: String? = "",
    @ColumnInfo(name = "body") var body: String? = ""

)