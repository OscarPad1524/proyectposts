package com.example.postsapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "geos_table")
data class GeosTable (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id: Long = 0,
    @ColumnInfo(name = "id_remote") var id_server: String? = "",
    @ColumnInfo(name = "id_user") var id_user: String? = "",
    @ColumnInfo(name = "latitud") var latitud: String? = "",
    @ColumnInfo(name = "longitud") var longitud: String? = ""

)