package com.example.repostapp.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.repostapp.database.models.UsersTable
import kotlinx.coroutines.flow.Flow

@Dao
interface UsersDao {

    @Query("SELECT * FROM users_table")
    suspend fun getAll(): MutableList<UsersTable>

    @Query("SELECT * FROM users_table")
    fun getAllOnFlow(): Flow<List<UsersTable>>


    @Query("SELECT * FROM users_table WHERE id_remote=:id")
    fun getById(id: String): UsersTable

    @Query("SELECT * FROM users_table WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<UsersTable>

    @Query("SELECT * FROM users_table WHERE name LIKE :name AND " +
            "username LIKE :userName LIMIT 1")
    fun findByName(name: String, userName: String): UsersTable

    @Query("SELECT * FROM users_table")
    fun getFlow(): Flow<List<UsersTable>>

    @Insert
    fun insert(user: UsersTable): Long

    //@Insert
    //fun insertAll(vararg users: UsersTable)

    @Delete
    fun delete(user: UsersTable)

}