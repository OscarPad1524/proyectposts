package com.example.postsapp.di

import android.content.Context
import com.example.repostapp.database.DatabaseManager
import com.example.repostapp.repo.PostTableRepo
import com.example.repostapp.repo.UserTableRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object UserModule {



   @Provides
   fun userTableRepoProvider(
       databaseManager: DatabaseManager
   ): UserTableRepo = UserTableRepo(databaseManager)

    @Provides
    fun postTableRepoProvider(
        databaseManager: DatabaseManager
    ): PostTableRepo = PostTableRepo(databaseManager)


}