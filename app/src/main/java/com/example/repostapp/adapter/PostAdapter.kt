package com.example.repostapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.postsapp.database.models.PostsTable
import com.example.repostapp.R

class PostAdapter(
    private var list: List<PostsTable>,
    private var onClick: (PostsTable, Int) -> Unit
) : RecyclerView.Adapter<PostAdapter.PostHolder>() {

    inner class PostHolder(val view: View) : RecyclerView.ViewHolder(view){

        var titulo: TextView = view.findViewById(R.id.tittle)

        fun render(post: PostsTable, position: Int){

            titulo.text = post.tittle

            view.setOnClickListener(){

                onClick(post, adapterPosition)

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostAdapter.PostHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PostHolder(layoutInflater.inflate(R.layout.item_post, parent, false))
    }

    override fun onBindViewHolder(holder: PostAdapter.PostHolder, position: Int) {
        holder.render(list[position], position)
    }

    override fun getItemCount(): Int = list.size

    fun insertList(lista: List<PostsTable>) {
        list = lista
        notifyDataSetChanged()
    }


}