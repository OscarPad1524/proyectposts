package com.example.repostapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.repostapp.R
import com.example.repostapp.database.models.UsersTable

class UserAdapter(
    var list: List<UsersTable> = listOf(),
    var onClick: (UsersTable, Int) -> Unit
) : RecyclerView.Adapter<UserAdapter.UsersHolder>() {



    inner class UsersHolder(val view: View) : RecyclerView.ViewHolder(view) {

        var nombre: TextView = view.findViewById(R.id.textViewName)

        fun render(user: UsersTable, position: Int){

            nombre.text = user.name

            view.setOnClickListener(){

                onClick(user, adapterPosition)

            }
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return UsersHolder(layoutInflater.inflate(R.layout.item_user, parent, false))
    }

    override fun onBindViewHolder(holder: UsersHolder, position: Int) {
        holder.render(list[position], position)
    }

    override fun getItemCount(): Int = list.size

    fun insertList(users: List<UsersTable>) {
        list = users
        notifyDataSetChanged()
    }

}