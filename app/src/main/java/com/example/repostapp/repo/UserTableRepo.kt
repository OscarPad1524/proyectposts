package com.example.repostapp.repo

import android.content.Context
import android.content.Intent
import android.content.LocusId
import android.widget.MultiAutoCompleteTextView
import androidx.core.content.ContextCompat.startActivity
import com.example.postsapp.database.models.AddressTable
import com.example.postsapp.database.models.CompanysTable
import com.example.postsapp.database.models.GeosTable
import com.example.postsapp.utils.Address
import com.example.repostapp.database.DatabaseManager
import com.example.repostapp.database.models.UsersTable
import com.example.repostapp.ui.post.PostsActivity
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow

class UserTableRepo(private val databaseManager: DatabaseManager){

    private var usersTableDao = databaseManager.usersDao()
    private var addressTableDao = databaseManager.addreesDao()
    private var geosTableDao = databaseManager.geosDao()
    private var companyTableDao = databaseManager.companysDao()

    fun insertAddress(address: AddressTable){
        addressTableDao.insert(address)
    }

    fun insertGeos(geo: GeosTable){
        geosTableDao.insert(geo)
    }

    fun insertCompany(compa: CompanysTable){
        companyTableDao.insert(compa)
    }

    fun getByIdString(id:String): UsersTable {
       return usersTableDao.getById(id)
    }

    fun getFlowUsers(): Flow<List<UsersTable>> = usersTableDao.getFlow()

    fun insert(user: UsersTable) {
        usersTableDao.insert(user)
    }


    fun getAll(): Flow<List<UsersTable>> {
        return usersTableDao.getAllOnFlow()
    }

    suspend fun getAllL(): MutableList<UsersTable> {
        return usersTableDao.getAll()
    }
}