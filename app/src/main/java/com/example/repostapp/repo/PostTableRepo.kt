package com.example.repostapp.repo

import com.example.postsapp.database.models.PostsTable
import com.example.repostapp.database.DatabaseManager
import kotlinx.coroutines.flow.Flow

class PostTableRepo(private val databaseManager: DatabaseManager) {

    private var postTableDao = databaseManager.postsDao()

    fun insert(post: PostsTable) = postTableDao.insert(post)

    suspend fun getAllById(id: String): MutableList<PostsTable> = postTableDao.getAllById(id)

    fun getFlow(id: String): Flow<List<PostsTable>> = postTableDao.getFlow(id)

    fun getById(id: Long): PostsTable = postTableDao.getByIdServer(id)

    suspend fun getAll():MutableList<PostsTable> = postTableDao.getAll()



}