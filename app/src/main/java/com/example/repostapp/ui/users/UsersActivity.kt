package com.example.repostapp.ui.users

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.repostapp.adapter.UserAdapter
import com.example.repostapp.database.models.UsersTable
import com.example.repostapp.databinding.ActivityUsersBinding
import com.example.repostapp.repo.UserTableRepo
import com.example.repostapp.ui.post.PostsActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class UsersActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUsersBinding
    private lateinit var lista: List<UsersTable>
    private lateinit var adapter: UserAdapter
    private val viewModel: UsersViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUsersBinding.inflate(layoutInflater)
        setContentView(binding.root)


        if(viewModel.isInternetAvailable(this)) {
            if(viewModel.confirm()) {
                Toast.makeText(this, "Agregando users", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "No se pudoa agregar los usuarios", Toast.LENGTH_SHORT).show()
            }
        }
        lista = mutableListOf()
        initRecycler()

        lifecycleScope.launch() {
            viewModel.getFlow().collect() {
                it
                //Toast.makeText(this@PostsActivity, "Cambiando", Toast.LENGTH_SHORT).show()
                adapter.insertList(it)
            }
            initRecycler()
        }

    }


    private fun initRecycler() {
        binding.recycler.layoutManager = LinearLayoutManager(this)
        adapter = UserAdapter(lista) { user, i -> onClic(user, i) }
        binding.recycler.adapter = adapter
    }

    fun onClic(user: UsersTable, i: Int) {

        val sendInfo: Intent = Intent(this, PostsActivity::class.java)
        sendInfo.putExtra("id", user.id_server)
        sendInfo.putExtra("name", user.name)
        sendInfo.putExtra("phone",user.phone)
        sendInfo.putExtra("email", user.email)
        startActivity(sendInfo)

    }

}