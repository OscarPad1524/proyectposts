package com.example.repostapp.ui.post

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.postsapp.database.models.PostsTable
import com.example.repostapp.repo.PostTableRepo
import com.example.repostapp.repo.UserTableRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class PostViewModel @Inject constructor(private var repo: PostTableRepo, private var userRepo: UserTableRepo ) : ViewModel() {

    val postLive: MutableLiveData<MutableList<PostsTable>> = MutableLiveData()


    suspend fun getAll() : MutableList<PostsTable> {
        return repo.getAll()
    }

    suspend fun getAllPostById(id: String) : MutableList<PostsTable>{

        return repo.getAllById(id)
    }

    fun getFlow(id : String) : Flow<List<PostsTable>> {
        return repo.getFlow(id)
    }


}
