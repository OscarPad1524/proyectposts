package com.example.repostapp.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.example.repostapp.databinding.ActivityMainBinding
import com.example.repostapp.ui.users.UsersActivity
import com.example.repostapp.database.DatabaseManager
import com.example.repostapp.database.models.UsersTable
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launch {
            if (viewModel.isInternetAvailable(this@MainActivity)){
                viewModel.verificacion()
            } else {
                Toast.makeText(this@MainActivity,
                    "No hay conexion a intenet, consultas limitadas",
                    Toast.LENGTH_SHORT).show()
            }
        }

        binding.cardOne.setOnClickListener{
            startActivity(Intent(this, UsersActivity::class.java))

        }
    }

}