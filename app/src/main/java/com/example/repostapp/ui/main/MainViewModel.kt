package com.example.repostapp.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.postsapp.database.models.AddressTable
import com.example.postsapp.database.models.CompanysTable
import com.example.postsapp.database.models.GeosTable
import com.example.postsapp.database.models.PostsTable
import com.example.postsapp.utils.AppService
import com.example.postsapp.utils.PostItem
import com.example.postsapp.utils.UserDataItem
import com.example.repostapp.database.models.UsersTable
import com.example.repostapp.repo.PostTableRepo
import com.example.repostapp.repo.UserTableRepo
import com.example.repostapp.retrofit.RestEngine
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private var userRepo: UserTableRepo,
    private var postRepo: PostTableRepo
    ) : ViewModel() {


    suspend fun verificacion() {
        if (userRepo.getAllL().isEmpty()) {
            callServiceGetUsers()
            callServiceGetPosts()
        }

    }

    private fun callServiceGetPosts() {
        val postService: AppService = RestEngine.getRestEngine().create(AppService::class.java)
        val result: retrofit2.Call<List<PostItem>> = postService.listPost()

        var respuesta: List<PostItem>? = listOf()


        result.enqueue(object : retrofit2.Callback<List<PostItem>>{
            override fun onFailure(
                call: retrofit2.Call<List<PostItem>>,
                t: Throwable
            ) {

            }

            override fun onResponse(
                call: retrofit2.Call<List<PostItem>>,
                response: Response<List<PostItem>>
            ) {
                respuesta = response.body()

                GlobalScope.launch {
                    for (i in respuesta!!) {

                        var post = PostsTable(
                            id_user = i.userId.toString(),
                            id_server = i.id.toLong(),
                            tittle = i.title,
                            body = i.body

                        )
                        postRepo.insert(post)
                    }
                }
            }
        }
        )
    }

    private fun callServiceGetUsers() {

        val userService: AppService = RestEngine.getRestEngine().create(AppService::class.java)
        val result: retrofit2.Call<List<UserDataItem>> = userService.listUsers()

        var respuesta: List<UserDataItem>? = listOf()


        result.enqueue(object : retrofit2.Callback<List<UserDataItem>>{
            override fun onFailure(
                call: retrofit2.Call<List<UserDataItem>>,
                t: Throwable
            ) {

                //En caso de fallar la consulta en la nube

            }

            override fun onResponse(
                call: retrofit2.Call<List<UserDataItem>>,
                response: Response<List<UserDataItem>>
            ) {
                respuesta = response.body()

                GlobalScope.launch {
                    for (i in respuesta!!) {

                        var user = UsersTable(
                            id_server = i.id.toString(),
                            name = i.name,
                            username = i.username,
                            email = i.email,
                            phone = i.phone,
                            website = i.website

                        )
                        var address = AddressTable(
                            id_server = i.id.toString(),
                            id_user = user.id_server,
                            street = i.address.street,
                            suite = i.address.suite,
                            city = i.address.city,
                            zipcode = i.address.zipcode

                        )
                        var geo = GeosTable(
                            id_server = i.id.toString(),
                            id_user = user.id_server,
                            latitud = i.address.geo.lat,
                            longitud = i.address.geo.lng,

                        )

                        var company = CompanysTable(
                            id_server = user.id_server,
                            id_user = user.id_server,
                            name = i.company.name,
                            catch_phrase = i.company.catchPhrase,
                            bs = i.company.bs
                        )

                        user.id_address = address.id
                        user.id_company = company.id


                        userRepo.insert(user)
                        userRepo.insertAddress(address)
                        userRepo.insertGeos(geo)
                        userRepo.insertCompany(company)


                    }
                }

            }
        }

        )


    }

    fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork
        val capabilities = connectivityManager.getNetworkCapabilities(network)
        return capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true
    }




}