package com.example.repostapp.ui.post

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.postsapp.database.models.PostsTable
import com.example.repostapp.adapter.PostAdapter
import com.example.repostapp.database.models.UsersTable
import com.example.repostapp.databinding.ActivityPostsBinding
import com.example.repostapp.ui.utils.DialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PostsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPostsBinding
    private lateinit var lista: MutableList<PostsTable>
    private lateinit var adapter: PostAdapter
    private val viewModel: PostViewModel by viewModels()
    private var user = UsersTable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPostsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        lista = mutableListOf()

        user.name = intent.extras?.getString("name")
        user.id_server = intent.extras?.getString("id")
        user.phone = intent.extras?.getString("phone")
        user.email = intent.extras?.getString("email")


        binding.nameEmpty.text = user.name
        binding.idEmpty.text = user.id_server
        binding.emailEmpty.text = user.email
        binding.phoneEmpty.text = user.phone

        lifecycleScope.launch {
            viewModel.getFlow(user.id_server!!).collect() {
                //Toast.makeText(this@PostsActivity, "Cambiando", Toast.LENGTH_SHORT).show()
                adapter.insertList(it)
            }
            initRecycler()
        }

    }

    private fun initRecycler() {
        binding.recycler.layoutManager = LinearLayoutManager(this)
        adapter = PostAdapter(lista) { post, i -> onClic(post, i) }
        binding.recycler.adapter = adapter
    }


    private fun onClic(post: PostsTable, i: Int) {
        var dialog = DialogFragment(post.tittle!!, post.body!!)
        dialog.show(supportFragmentManager, "dialog")
    }

}